const { Load, schemaJoiLoad } = require("../models/Load");
const { Truck } = require("../models/Truck");

const getDate = require("../helpers/getDate");

const loadSizes = {
  SPRINTER: {
    width: 300,
    length: 250,
    height: 170,
  },
  "SMALL STRAIGHT": {
    width: 500,
    length: 250,
    height: 170,
  },
  "LARGE STRAIGHT": {
    width: 700,
    length: 350,
    height: 200,
  },
};
const loadState = [
  "En route to Pick Up",
  "Arrived to Pick Up",
  "En route to delivery",
  "Arrived to delivery",
];

const createLoad = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

    await schemaJoiLoad.validateAsync({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });

    const load = new Load({
      created_by: userId,
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
      created_date: getDate(),
    });

    load
      .save()
      .then((saved) =>
        res.status(200).json({
          message: "Load created successfully",
        })
      )
      .catch((err) => {
        console.log(err);
        res.status(400).json({ message: "Load save error" });
      });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const getLoads = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const { status = undefined, limit = 0, offset = 0 } = req.query;

    let loads = await Load.find({ created_by: userId }, "-__v");

    if (offset !== 0 && limit !== 0) {
      loads = loads.slice(Number(offset), Number(offset) + Number(limit));
    }
    if (status) {
      loads = loads.filter((el) => el.status === status);
    }

    res.status(200).json({
      loads: loads,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const getLoadsActiveForDriver = async (req, res, next) => {
  try {
    const { userId } = req.user;

    const truck = await Truck.findOne({ created_by: userId, status: "OL" });
    if (!truck) throw Error(`user dont have load`);

    const load = await Load.findOne(
      { _id: truck.assigned_to, assigned_to: userId, status: "ASSIGNED" },
      "-__v"
    );

    res.status(200).json({ load });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const getUserLoadById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const load = await Load.findOne({ _id: id }, "-__v");

    res.status(200).json({ load });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const getLoadShippingInfoById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const userLoad = await Load.findOne({ _id: id }, "-__v");

    const LoadTruck = await Truck.findOne(
      { created_by: userLoad.assigned_to, assigned_to: id },
      "-__v"
    );

    res.status(200).json({ load: userLoad, truck: LoadTruck });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const postUserLoad = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;

    const load = await Load.findOne({ _id: id }, "-__v");

    if (load.status !== "NEW") {
      return res.status(200).json({
        message: "Load posted successfully",
        driver_found: true,
      });
    }

    load.status = "POSTED";
    await load.save();

    const trucks = await Truck.find({ status: "IS" }, "-__v");
    if (trucks.length === 0) {
      load.status = "NEW";
      load.logs.push({ message: "Not found free truck", time: getDate() });

      await load.save();

      return res.status(200).json({
        message: "Load posted successfully",
        driver_found: false,
      });
    }

    for (let i = 0; i < trucks.length; i++) {
      const truck = trucks[i];
      const userTruks = await Truck.find({ created_by: truck.created_by, status: "OL" });
      const verifyTruck = userTruks.length === 0;

      if (
        load.dimensions.width < loadSizes[truck.type].width &&
        load.dimensions.length < loadSizes[truck.type].length &&
        load.dimensions.height < loadSizes[truck.type].height &&
        verifyTruck
      ) {
        const findedTruck = await Truck.findById(truck._id);
        findedTruck.status = "OL";
        findedTruck.assigned_to = load._id;
        await findedTruck.save();

        load.status = "ASSIGNED";
        load.state = "En route to Pick Up";
        load.assigned_to = findedTruck.created_by;
        load.logs.push({ message: "Load assigned", time: getDate() });
        await load.save();

        return res.status(200).json({
          message: "Load posted successfully",
          driver_found: true,
        });
      }
    }

    load.status = "NEW";
    await load.save();

    res.status(200).json({
      message: "Load posted successfully",
      driver_found: false,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const patchLoadState = async (req, res, next) => {
  try {
    const { userId } = req.user;

    const load = await Load.findOne({ assigned_to: userId, status: "ASSIGNED" }, "-__v");
    if (!load)
      return res.status(200).json({ message: `Load state changed to 'Arrived to delivery'` });

    const currentStateIndex = loadState.findIndex((el) => load.state === el);
    const newLoadState = loadState[currentStateIndex + 1];

    if (newLoadState) {
      load.state = newLoadState;
      load.logs.push({ message: "Load is " + newLoadState, time: getDate() });
    }
    if (newLoadState === "Arrived to delivery") {
      load.status = "SHIPPED";
      load.assigned_to = "undefined";

      const truck = await Truck.findOne({ created_by: userId, status: "OL" });
      truck.status = "IS";
      await truck.save();
    }

    await load.save();

    res.status(200).json({ message: `Load state changed to '${newLoadState}'` });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const putLoadInfo = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { name, payload, pickup_address, delivery_address, dimensions } = req.body;

    await schemaJoiLoad.validateAsync({
      name,
      payload,
      pickup_address,
      delivery_address,
      dimensions,
    });

    const load = await Load.findById(id);
    if (load.status !== "NEW") throw Error("Load in work process");

    load.name = name;
    load.payload = payload;
    load.pickup_address = pickup_address;
    load.delivery_address = delivery_address;
    load.dimensions = dimensions;

    await load.save();

    res.status(200).json({
      message: "Load details changed successfully",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const deleteLoadById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const load = await Load.findById(id);
    if (load.status !== "NEW") throw Error("Load in work process");

    await load.delete();

    res.status(200).json({
      message: "Load deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

module.exports = {
  createLoad,
  getLoads,
  getLoadsActiveForDriver,
  getUserLoadById,
  getLoadShippingInfoById,
  postUserLoad,
  patchLoadState,
  putLoadInfo,
  deleteLoadById,
};
