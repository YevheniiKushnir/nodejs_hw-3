const bcrypt = require("bcryptjs");
const Joi = require("joi");
const jwt = require("jsonwebtoken");
const Sib = require("sib-api-v3-sdk");
const { v4: uuidv4 } = require("uuid");

const { schemaJoiPerson, Person } = require("../models/Person");

const getDate = require("../helpers/getDate");

const schemaJoiLogin = Joi.object({
  email: Joi.string()
    .email({ minDomainSegments: 2, tlds: { allow: ["com", "net"] } })
    .error(new Error("Email must be valid")),
  password: Joi.string()
    .pattern(/^[a-zA-Z0-9]{3,40}$/i)
    .error(new Error("Password must be valid")),
});

const register = async (req, res, next) => {
  try {
    const { email, password, role } = req.body;

    await schemaJoiPerson.validateAsync({ email, password, role });

    const person = new Person({
      created_date: getDate(),
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });

    person
      .save()
      .then((saved) =>
        res.status(200).json({
          message: "Profile created successfully",
        })
      )
      .catch((err) => {
        res.status(400).json({ message: "User exist" });
      });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const login = async (req, res, next) => {
  try {
    const { email, password } = req.body;

    await schemaJoiLogin.validateAsync({ email, password });

    const user = await Person.findOne({ email });
    if (!user) throw Error("User not exist");

    const comparePasswords = await bcrypt.compare(String(password), String(user.password));

    if (user && comparePasswords) {
      const payload = { email: user.email, userId: user._id, role: user.role };

      const jwtToken = jwt.sign(payload, process.env.JWT_SECRET_KEY);

      return res.status(200).json({
        jwt_token: jwtToken,
      });
    } else throw Error("Not authorized");
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const resetPassword = (req, res, next) => {
  try {
    const { email } = req.body;

    const newPass = uuidv4().split("-").join("");

    const client = Sib.ApiClient.instance;
    const apiKey = client.authentications["api-key"];
    apiKey.apiKey = process.env.EMAIL_KEY;

    const tranEmailApi = new Sib.TransactionalEmailsApi();
    const sender = {
      email: "kushnir.open.test@gmail.com",
      name: "hw-3_kushnir",
    };
    const receivers = [
      {
        email: email,
      },
    ];

    tranEmailApi
      .sendTransacEmail({
        sender,
        to: receivers,
        subject: "Reset password",
        textContent: `
        Now use new password for login: <{{params.newPass}}>.
        `,
        params: {
          newPass: newPass,
        },
      })
      .then(async () => {
        const person = await Person.findOne({ email: email });

        (person.password = await bcrypt.hash(newPass, 10)), await person.save();

        res.status(200).json({
          message: "New password sent to your email address",
        });
      })
      .catch(() => {
        res.status(500).json({ message: "Server error" });
      });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

module.exports = {
  register,
  login,
  resetPassword,
};
