const bcrypt = require("bcryptjs");

const { Person } = require("../models/Person");

const getUserData = async (req, res, next) => {
  try {
    const { userId } = req.user;

    const { _id, email, created_date, role } = await Person.findById(userId);

    res.json({
      user: {
        _id,
        role,
        email,
        created_date,
      },
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const changeUserPassword = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const { oldPassword, newPassword } = req.body;

    const user = await Person.findById(userId);

    const verifyUser = bcrypt.compare(String(oldPassword), String(user.password));

    if (newPassword && verifyUser) {
      const hashedPassword = await bcrypt.hash(newPassword, 10);
      user.password = hashedPassword;

      await user.save();
    } else throw Error("Access error");

    res.json({
      message: "Password changed successfully",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const deleteUserData = async (req, res, next) => {
  try {
    const { userId } = req.user;

    const user = await Person.findById(userId);

    await user.delete();

    res.json({
      message: "Profile deleted successfully",
    });
  } catch (error) {
    res.status({ message: error.message || "Bad request" });
  }
};

module.exports = {
  getUserData,
  changeUserPassword,
  deleteUserData,
};
