const { Truck } = require("../models/Truck");

const getDate = require("../helpers/getDate");

const createTruck = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const { type } = req.body;

    const truck = new Truck({
      created_date: getDate(),
      created_by: userId,
      type,
    });

    truck
      .save()
      .then((saved) =>
        res.status(200).json({
          message: "Truck created successfully",
        })
      )
      .catch((err) => {
        console.log(err);
        res.status(400).json({ message: "Truck save error" });
      });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const getTrucks = async (req, res, next) => {
  try {
    const { offset = 0, limit = 0 } = req.query;

    const { userId } = req.user;
    const trucks = await Truck.find({ created_by: userId }, "-__v");

    let newTrucks = null;
    if (offset !== 0 && limit !== 0) {
      newTrucks = trucks.slice(Number(offset), Number(offset) + Number(limit));
    }

    res.status(200).json({
      trucks: newTrucks ?? trucks,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const getTruckById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const truck = await Truck.findById({ _id: id }, "-__v");
    if (!truck) throw Error("Truck not exist");

    res.status(200).json({
      truck,
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

const changeTruckById = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { type } = req.body;

    const truck = await Truck.findById({ _id: id }, "-__v");

    truck.type = type;

    await truck.save();

    res.status(200).json({
      message: "Truck details changed successfully",
    });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const assignTruckById = async (req, res, next) => {
  try {
    const { id } = req.params;
    const { userId } = req.user;

    const truck = await Truck.findById({ _id: id }, "-__v");

    truck.assigned_to = userId;

    await truck.save();

    res.status(200).json({
      message: "Truck assigned successfully",
    });
  } catch (error) {
    res.status(400).json({ message: error.message || "Bad request" });
  }
};

const deleteTruckById = async (req, res, next) => {
  try {
    const { id } = req.params;

    const truck = await Truck.findById({ _id: id }, "-__v");
    if (!truck) throw Error("Truck not exist");

    await truck.delete();

    res.status(200).json({
      message: "Truck deleted successfully",
    });
  } catch (error) {
    res.status(400).json({
      message: error.message || "Bad request",
    });
  }
};

module.exports = {
  createTruck,
  getTrucks,
  getTruckById,
  deleteTruckById,
  changeTruckById,
  assignTruckById,
};
