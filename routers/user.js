const express = require("express");
const Router = express.Router();

const { getUserData, changeUserPassword, deleteUserData } = require("../services/user");

Router.get("/me", getUserData);

Router.patch("/me/password", changeUserPassword);

Router.delete("/me", deleteUserData);

module.exports = Router;
