const express = require("express");
const Router = express.Router();

const { register, login, resetPassword } = require("../services/auth");

Router.post("/register", register);
Router.post("/login", login);
Router.post("/forgot_password", resetPassword);

module.exports = Router;
