const express = require("express");
const Router = express.Router();

const checkRole = require("../middlewares/checkRole");

const {
  createLoad,
  getLoads,
  getLoadsActiveForDriver,
  getUserLoadById,
  getLoadShippingInfoById,
  postUserLoad,
  patchLoadState,
  putLoadInfo,
  deleteLoadById,
} = require("../services/load");

Router.get("/", getLoads);
//D
Router.get("/active", checkRole("DRIVER"), getLoadsActiveForDriver);
Router.get("/:id", checkRole("SHIPPER"), getUserLoadById);
Router.get("/:id/shipping_info", checkRole("SHIPPER"), getLoadShippingInfoById);

Router.post("/", checkRole("SHIPPER"), createLoad);
Router.post("/:id/post", checkRole("SHIPPER"), postUserLoad);
Router.put("/:id", checkRole("SHIPPER"), putLoadInfo);

//D
Router.patch("/active/state", checkRole("DRIVER"), patchLoadState);

Router.delete("/:id", checkRole("SHIPPER"), deleteLoadById);

module.exports = Router;
