const mongoose = require("mongoose");

const truck = new mongoose.Schema({
  created_date: {
    type: String,
    required: true,
  },
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: "undefined",
  },
  type: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: "IS",
  },
});

const Truck = mongoose.model("Trucks", truck);

module.exports = { Truck };
