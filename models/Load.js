const mongoose = require("mongoose");
const Joi = require("joi");

const schemaJoiLoad = Joi.object({
  name: Joi.string().required().error(new Error("Empty name")),
  payload: Joi.number().required().error(new Error("Empty payload")),
  pickup_address: Joi.string().required().error(new Error("Empty pickup_address")),
  delivery_address: Joi.string().required().error(new Error("Empty delivery_address")),
  dimensions: Joi.object({
    width: Joi.number().required().error(new Error("Error width in dimensions")),
    length: Joi.number().required().error(new Error("Error length in dimensions")),
    height: Joi.number().required().error(new Error("Error height in dimensions")),
  }).error(new Error("Empty dimensions")),
});

const load = new mongoose.Schema({
  created_date: {
    type: String,
    required: true,
  },
  created_by: {
    type: String,
    required: true,
  },
  assigned_to: {
    type: String,
    default: "undefined",
  },
  name: {
    type: String,
    required: true,
  },
  status: {
    type: String,
    default: "NEW",
  },
  state: {
    type: String,
    default: "undefined",
  },
  payload: {
    type: Number,
    required: true,
  },
  pickup_address: {
    type: String,
    required: true,
  },
  delivery_address: {
    type: String,
    required: true,
  },
  dimensions: {
    type: Object,
    required: true,
    width: {
      type: Number,
      required: true,
    },
    length: {
      type: Number,
      required: true,
    },
    height: {
      type: Number,
      required: true,
    },
  },
  logs: {
    type: Array,
    default: [],
  },
});

const Load = mongoose.model("Loads", load);

module.exports = { Load, schemaJoiLoad };
